/*============================================
ACTIVITY 

MODULE: WD004-S9-DATA-STRUCTURE-4
GitLab: s9-js-data-structure-4

1) Create a recursive function called "sum" that displays that sum of consecutive numbers so that
console.log(sum(10)) will display 55:
1+2+3+4+5+6+7+8+9+10 = 55;

============================================*/

console.log('SUM ******************************');
function sum(num) {
	if(num !== 0) {
		return num + sum(num-1); 
	}
	return num;
}

console.log("Sum = " + sum(10));




/*=========================================== 
2) Create a recursive function called "sumOdd" that displays that sum of odd numbers so that
console.log(sum(50)) will display 625:
============================================*/
console.log('SUM ODD******************************');

//let count = num;
function sumOdd(count) {
	if(count !== 0) {
		if(count%2 === 0) {
			return sumOdd(count-1);
		} else {
			//console.log("odd = " + count);
			return count + sumOdd(count-1); 
		}
	} else {
		return count;
	}
}

console.log("Sum Odd = " + sumOdd(50));

/*=========================================== 
3) Create a recursive function called "sumEven" that displays that sum of even numbers so that
console.log(sum(50)) will display 650:
============================================*/

console.log('SUM EVEN******************************');

//let count = num;
function sumEven(count) {
	if(count !== 0) {
		if(count%2 === 0) {
			//console.log("even = " + count);
			return count + sumEven(count-1);
		} else {
			return sumEven(count-1);
		}
	} else {
		return count;
	}
}

console.log("Sum Odd = " + sumEven(50));




console.log('******************************');
/*============================================
/*============================================
STRETCH GOAL (Put in script4.js)

MODULE: WD004-S9-DATA-STRUCTURE-4
GitLab: s9-js-data-structure-4

The Fibonacci Sequence (aka Golden Ratio) is the series of numbers:

0, 1, 1, 2, 3, 5, 8, 13 ...

The next number is found by adding up the two numbers before it.

2 is found by adding the two numbers before it (1+1)
3 is found by adding the two numbers before it (1+2),
5 is found by adding the two numbers before it (2+3),
8 is found by adding the two numbers before it (3+5),
13 is found by adding the two numbers before it (5+8),
and so on!

Another way to see it is shown below:

1. 0
2. 1
3. 1
4. 2
5. 3
6. 5
7. 8
8. 13

If you're looking for the 3rd number, it's 1.
If you're looking for the 6th number, it's 5.
If you're looking for the 8th number, it's 13.
and so on!

Create a function called fibonacci that gives the total of numbers up to the given value of the parameter.
eg. fibonacci(8) === 13.

============================================*/