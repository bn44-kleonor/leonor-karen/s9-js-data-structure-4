/*============================================
ACTIVITY 

MODULE: WD004-S9-DATA-STRUCTURE-4
GitLab: s9-js-data-structure-4

/*============================================
STRETCH GOAL (Put in script4.js)

MODULE: WD004-S9-DATA-STRUCTURE-4
GitLab: s9-js-data-structure-4

The Fibonacci Sequence (aka Golden Ratio) is the series of numbers:

0, 1, 1, 2, 3, 5, 8, 13 ...

The next number is found by adding up the two numbers before it.

2 is found by adding the two numbers before it (1+1)
3 is found by adding the two numbers before it (1+2),
5 is found by adding the two numbers before it (2+3),
8 is found by adding the two numbers before it (3+5),
13 is found by adding the two numbers before it (5+8),
and so on!

Another way to see it is shown below:

1. 0
2. 1
3. 1
4. 2
5. 3
6. 5
7. 8
8. 13

If you're looking for the 3rd number, it's 1.
If you're looking for the 6th number, it's 5.
If you're looking for the 8th number, it's 13.
and so on!

Create a function called fibonacci that gives the total of numbers up to the given value of the parameter.
eg. fibonacci(8) === 13.

============================================*/

// console.log('Fibonacci ******************************');

// let i = 0;

// function fibonacci(count){
//   console.log(count);
//   if(count !== 0) {
//     //i = i + (i+1);

//     temp = i;
//     i = i + temp;
//     i++;

//     return count + fibonacci(count-1);
//   } else {
//     return count;
//   }
// }

// console.log(fibonacci(8));

// count = 8;
// let fibonacci = 1;
// let temp1;
// let temp2;

// for(let i=2; i <= count; i++) {
//   if(i<=2){
//     fibonacci = fibonacci + i;
//     console.log("1st if " + fibonacci);
//     temp1 = i;
//     temp2 = fibonacci;
//     i++;
//     console.log("     i is = " + i);
//   } else {
//   //if(i > 3){
//     fibonacci = temp2 + temp1;
//     temp1 = temp2;
//     temp2 = fibonacci;
//     console.log("fibonacci is " + fibonacci);
//     //temp = fibonacci;
//     i++;
//     console.log("     i is = " + i);
//   //}
//   }
// }

// console.log(count);

/*
2 is found by adding the two numbers before it (1+1)
3 is found by adding the two numbers before it (1+2),
5 is found by adding the two numbers before it (2+3),
8 is found by adding the two numbers before it (3+5),
13 is found by adding the two numbers before it (5+8),
and so on!

1. 0
2. 1
3. 1
4. 2
5. 3
6. 5
7. 8
8. 13

*/

let fibonaccinum = 1;
let temp1;
let temp2;

function fibonacci(count){
  tempCount = count;
  count = count-2;
  for(let i=2; i <= count+1; i++) {
    if(i<=2){
      fibonaccinum = fibonaccinum + i;
      console.log("1st is " + fibonaccinum);
      temp1 = i;
      temp2 = fibonaccinum;
      i++;
      //console.log("     i is = " + i);
    } else if(i<=count) {
    //if(i > 3){
      fibonaccinum = temp2 + temp1;
      temp1 = temp2;
      temp2 = fibonaccinum;
      console.log("fibonacci temp count is " + fibonaccinum);
      //temp = fibonaccinum;
      //i++;
      console.log("     i is = " + i);
    //}
    } else {
      return fibonaccinum;
    }
  }
}

console.log("Fibonacci is " + fibonacci(9));


/* Fib=0,1,1,2,3,5,8,13,21,34,55,89,... */

