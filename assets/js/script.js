/*============================================
GUIDED ACTIVITY

MODULE: WD004-S8-DATA-STRUCTURE-3


RECURSION 
A programming term that means calling a function from itself.

Example:
Factorial of a number n is the product of all positive integers 
less than or equal to n. Hence,

4! = 4 × 3 × 2 × 1 = 24
============================================*/


function factorial(x) {
  if(x > 1) {
    return x * factorial(x-1)
  }
  return x;
}

console.log("factorial is " + factorial(5));


//similar to
// function factorial(x) {
//   if(x > 1) {
//     return x * factorial(x-1)
//   } else {
//     return x;
//   }
// }

// console.log("factorial is " + factorial(5));


/*============================================
Example 2:

Exponents are shorthand for repeated multiplication of the same thing by itself. Hence,

5 raise to the 3rd power or 5 cubed = 5 x 5 x 5 = 125

Note: any number, except for 0, raised to 0 power ==== 1

============================================*/

console.log("************************************");
// function power(a, b) {
//   console.log("a = " + a);
//   console.log("b = " + b);
//   if(b > 1) {
//     a = a*5;
//     b--;
//     power(a,b);
//   }
//   return 1;
// }

// //console.log((5)^2);
// console.log(power(5,3));


// function power(a, b) {
//   console.log("a = " + a);
//   console.log("b = " + b);
//   if(a !== 0 && b === 0) {
//     return 1;
//   } else {
//     a = a*5;
//     b--;
//     power(a,b);
//   }
//   //return a;
// }

// //console.log((5)^2);
// console.log(power(5,3));


// the ans
function power(a, b) {
  console.log("a = " + a);
  console.log("b = " + b);
  if(a !== 0 && b === 0) {
    return 1;
  }
  return a * power(a, b-1);
}

//console.log((5)^2);
console.log(power(5,3));




/*====================================================*/
/*============================================
ACTIVITY

MODULE: WD004-S8-DATA-STRUCTURE-3
GitLab: s8-js-data-structure-3

Create a function that checks if a given work is a WORD palindrome or not.
A palindrome is a word, phrase, or sequence that reads the same backward as forward.
Examples are: madam, anna, hannah, racecar

Note: The function will be limited to checking WORD palindromes only.

1. Create a function called "checkPalindrome" that accepts a string as a parameter.
2. If the string has 1 or no character, display the console - It's a palindrome - and then return true. 
3. Otherwise, check if the first and last characters of the string are equal.
4. If they are, remove the first and last characters using slice method and return the same function (i.e., employ recursion). 
5. If they're not, display in the console - It's not a palindrome - and then return false.
============================================*/

console.log("************************************");

function checkPalindrome(str){
  console.log(str);
  //let str.length;
  if(str.length <= 1) {
    console.log("It's a palindrome.");
    return true;
  } else {
    if(str[0] === str[str.length-1]) {
      //let temp = str.slice(1,-1);
      return checkPalindrome(str.slice(1,-1));
    } else {
      console.log("It's not a palindrome.");
      false;
    }
  }
}


console.log(checkPalindrome("madam"));



// to know the 1st and last char
//let str = "madam";
//console.log(str);           //1st character of string
//console.log(str.length-1);  //last character of string



